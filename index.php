<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Report</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">
    <style>
      .item{
        font-size:20px;
        margin:5px;
      }
    </style>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script>
      var unit,item,selected,amount,user="066",station="station 24";
      $(document).ready(function(){
        getList();

      });

      //sets submit on enter key for input
      function setEnter(){
        document.getElementById('amount').onkeypress = function(e){
          if (!e) e = window.event;
          var keyCode = e.keyCode || e.which;
          if (keyCode == '13'){
            submit();// Enter pressed
            return false;
          }
        }
      }

      function itemSelect(btn){
        selected = btn;
        item = btn.innerHTML;
        unit = btn.attributes.unit.nodeValue;
        $("#msg").html(item);
        $("#msg").append("<br><input id='amount' type='number'/> "+unit);
        $("#modal").modal();
        setTimeout(function(){
          $('#amount').focus();
          setEnter();
        },500);
      }

      function submit(){
        $.get("submit.php",{unit:unit,
          item:item,
          amount:amount,
          user:user,
          station:station},function(data){
          console.log(data);
          selected.hidden = true;
          $("#modal").modal("hide");
        });
      }

      function getList(){
        $("#main").html("");
        $.get("list.php",function(data){
          var list = data.split("\n");
          for(var i of list){
            if( i != "" ){
              var item = i.split("|")[0];
              var unit = i.split("|")[1];
              $("#main").append(`<div class="row">
                <button onclick="itemSelect(this)" type="button" unit="`+unit+`" class="item btn btn-primary btn-block">`
                  +item+
                  `</button>
              </div>`);
            }

          }
        });
      }

    </script>
  </head>
  <body>

    <div id="main" class="container">

    </div>

    <!----------------Begin Modal------------>
    <div class="container">

      <!-- Modal -->
      <div class="modal fade" id="modal" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Report</h4>
            </div>
            <div id="msg" class="modal-body">

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button onclick="submit()" type="button" class="btn btn-primary">Submit</button>
            </div>
          </div>

        </div>
      </div>

    </div>
    <!----------------End Modal------------>

  </body>
</html>

